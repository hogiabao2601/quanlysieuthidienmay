/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import DAO.NewHibernateUtil;
import POJO.Nhatky;
import java.util.ArrayList;
import java.util.Date;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Ho Gia Bao
 */
public class DB_Utils {

    public NewHibernateUtil hibernateUtil;
    SessionFactory factory;

    public DB_Utils() {
        factory = NewHibernateUtil.getSessionFactory();
    }

    public ArrayList<?> Select(String hql) {
        ArrayList<?> listObj = null;
        Session session = factory.openSession();
        Query query = session.createQuery(hql);
        listObj = (ArrayList<?>) query.list();
        return listObj;
    }

    public void WriteLog(String action, String tableName, String content, String username, String category) {
        Session session = factory.openSession();
        org.hibernate.Transaction transaction = null;
        Nhatky nhatKy = new Nhatky();
        try {
            nhatKy.setDanhmuc(category);
            nhatKy.setGhichu(content);
            nhatKy.setHanhdong(action);
            nhatKy.setNgay(new Date());
            nhatKy.setNhanvien(username);
            nhatKy.setTenbang(tableName);
            transaction = session.beginTransaction();
            session.save(nhatKy);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            System.err.println(e);
        } finally {
            session.close();
        }
    }

    public boolean Insert(Object obj) {
        Session session = factory.openSession();
        org.hibernate.Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(obj);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            System.out.println(e);
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    public boolean Update(Object obj) {
        
        
        Session session = factory.openSession();
        org.hibernate.Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(obj);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            System.out.println(e);
            return false;
        } finally {
            session.close();
        }
        return true;
    }
}
