/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ho Gia Bao
 */
public class Variables {
    
    public static final int NAM = 0;
    public static final int NU = 1;
    
    public static final String INSERT = "INSERT", 
            UPDATE = "UPDATE",
            DELETE = "DELETE";

    public static final int ADMIN = 0, NHANVIEN = 1;    
    
    public static final Map<String, String> ADMIN_FUNCTION;
    static
    {
        ADMIN_FUNCTION = new HashMap<>();
        ADMIN_FUNCTION.put("BanHang", "Bán Hàng");
        ADMIN_FUNCTION.put("MuaHang", "Mua Hàng");
    }
    

//    public  static final ArrayList<MenuItem> = new ArrayList<MenuItem>(
//        Arrays.asList(new MenuItem("BanHang", "Bán Hàng")));
//    
//     ArrayList<String> obj = new ArrayList<String>(
//		Arrays.asList("Pratap", "Peter", "Harsh"));
//     
//            
//            
//             ArrayList<String> cities = new ArrayList<String>(){{
//		   add("Delhi");
//		   add("Agra");
//		   add("Chennai");
//		   }};
//	  System.out.println("Content of Array list cities:"+cities);
//          
//    public static final Map<String, String> ADMIN_FUNCTION;
//    static {
//        Map<String, String> aMap = new HashMap<>();
//        aMap.put("BanHang", "Bán Hàng");
//        ADMIN_FUNCTION = Collections.unmodifiableMap(aMap);
//    }
//    
//    public static final Map<String, String> NHANVIEN_FUNCTION;
//    static {
//        Map<String, String> aMap = new HashMap<>();
//        aMap.put("BanHang", "Bán Hàng");
//        NHANVIEN_FUNCTION = Collections.unmodifiableMap(aMap);
//    }
}
