/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

public class MenuItem {
 private String name;
    private String text;

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

   

    public MenuItem(String name, String text) {
        this.text = text;
        this.name = name;
    }
}
