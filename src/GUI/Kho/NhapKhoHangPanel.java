/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Kho;

import DAO.ChiTietPhieuNhapKhoDAO;
import DAO.PhieuNhapKhoDAO;
import DAO.NhaCungCapDAO;
import DAO.SanPhamDAO;
import POJO.Chitietphieunhapkho;
import POJO.Nhacungcap;
import POJO.Nhanvien;
import POJO.Phieunhapkho;
import POJO.Sanpham;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class NhapKhoHangPanel extends javax.swing.JPanel {

    public NhapKhoHangPanel(Nhanvien nhanVien) {
        initComponents();
        this.nhanVien = nhanVien;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbChiTietNhapKho = new javax.swing.JTable();
        btnThemSanPham = new javax.swing.JButton();
        btnThayDoiPhieuNhapKho = new javax.swing.JButton();
        tfMaPhieuNhap = new javax.swing.JTextField();
        cbNhaCungCap = new javax.swing.JComboBox();
        tfSoLuong = new javax.swing.JTextField();
        tfDonGia = new javax.swing.JTextField();
        cbSanPham = new javax.swing.JComboBox();
        btnXoaSanPham = new javax.swing.JButton();
        btnThemPhieuNhapKho = new javax.swing.JButton();
        tfNgayNhap = new com.toedter.calendar.JDateChooser();
        btnSuaSanPham = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbPhieuNhapKho = new javax.swing.JTable();

        addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                formAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        setLayout(new java.awt.BorderLayout());

        jPanel1.setPreferredSize(new java.awt.Dimension(617, 30));

        jLabel1.setText("Nhập kho");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(546, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(287, 287, 287))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        jLabel2.setText("Ngày nhập:");

        jLabel3.setText("Nhà cung cấp:");

        jLabel4.setText("Danh sách sản phẩm:");

        jLabel5.setText("Số lượng:");

        jLabel6.setText("Đơn giá:");

        jLabel7.setText("Mã phiếu nhập kho:");

        tbChiTietNhapKho.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Tên sản phâm", "Số lượng", "Đơn giá"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbChiTietNhapKho.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbChiTietNhapKhoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbChiTietNhapKho);

        btnThemSanPham.setText("Thêm sản phẩm");
        btnThemSanPham.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemSanPhamActionPerformed(evt);
            }
        });

        btnThayDoiPhieuNhapKho.setText("Thay đổi phiếu nhập kho");
        btnThayDoiPhieuNhapKho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThayDoiPhieuNhapKhoActionPerformed(evt);
            }
        });

        cbNhaCungCap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbNhaCungCapActionPerformed(evt);
            }
        });

        tfSoLuong.setPreferredSize(new java.awt.Dimension(50, 20));

        tfDonGia.setMinimumSize(new java.awt.Dimension(100, 20));
        tfDonGia.setPreferredSize(new java.awt.Dimension(100, 20));

        btnXoaSanPham.setText("Xóa sản phẩm");
        btnXoaSanPham.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaSanPhamActionPerformed(evt);
            }
        });

        btnThemPhieuNhapKho.setText("Thêm phiếu nhập kho");
        btnThemPhieuNhapKho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemPhieuNhapKhoActionPerformed(evt);
            }
        });

        tfNgayNhap.setDateFormatString("dd-MM-yyyy");

        btnSuaSanPham.setText("Sửa sản phẩm");
        btnSuaSanPham.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaSanPhamActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnThemSanPham)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnXoaSanPham)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSuaSanPham))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnThemPhieuNhapKho)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnThayDoiPhieuNhapKho))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cbSanPham, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jLabel5)
                            .addGap(14, 14, 14)
                            .addComponent(tfSoLuong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel6)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(tfDonGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbNhaCungCap, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tfMaPhieuNhap, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tfNgayNhap, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(jLabel2)
                        .addComponent(tfMaPhieuNhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tfNgayNhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbNhaCungCap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(tfSoLuong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfDonGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbSanPham, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnThemSanPham)
                    .addComponent(btnXoaSanPham)
                    .addComponent(btnSuaSanPham))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnThemPhieuNhapKho)
                    .addComponent(btnThayDoiPhieuNhapKho))
                .addContainerGap(169, Short.MAX_VALUE))
        );

        add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel3.setPreferredSize(new java.awt.Dimension(300, 549));
        jPanel3.setLayout(new java.awt.BorderLayout());

        jPanel4.setPreferredSize(new java.awt.Dimension(300, 50));

        jLabel8.setText("Danh sách phiếu nhập kho");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(jLabel8)
                .addContainerGap(93, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jPanel3.add(jPanel4, java.awt.BorderLayout.NORTH);

        jPanel5.setLayout(new java.awt.BorderLayout());

        tbPhieuNhapKho.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Mã phiếu nhập kho", "Ngày nhập", "Người nhập"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbPhieuNhapKho.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbPhieuNhapKhoMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbPhieuNhapKho);

        jPanel5.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jPanel3.add(jPanel5, java.awt.BorderLayout.CENTER);

        add(jPanel3, java.awt.BorderLayout.LINE_START);
    }// </editor-fold>//GEN-END:initComponents

    private void btnThayDoiPhieuNhapKhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThayDoiPhieuNhapKhoActionPerformed
        // TODO add your handling code here:
        if (tbPhieuNhapKho.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Chưa chọn phiếu nhập kho");
            return;
        }
        int index = tbPhieuNhapKho.getSelectedRow();
        PhieuNhapKhoFrame frm = new PhieuNhapKhoFrame(this, listPhieuNhapKho.get(index), nhanVien);
        frm.setVisible(true);
    }//GEN-LAST:event_btnThayDoiPhieuNhapKhoActionPerformed

    private void formAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_formAncestorAdded
        LoadPhieuNhapKho();
        LoadNhanCungCap();
    }//GEN-LAST:event_formAncestorAdded

    private void tbPhieuNhapKhoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbPhieuNhapKhoMouseClicked
        // TODO add your handling code here:
        int index = tbPhieuNhapKho.getSelectedRow();
        Phieunhapkho phieuNhapKho = listPhieuNhapKho.get(index);
        LoadChiTietPhieuNhapKho(phieuNhapKho);

        tfMaPhieuNhap.setText(phieuNhapKho.getMaphieunhap());
        tfNgayNhap.setDate(phieuNhapKho.getNgaynhap());
        cbNhaCungCap.setSelectedItem(phieuNhapKho.getNhacungcap().getTennhacungcap());
    }//GEN-LAST:event_tbPhieuNhapKhoMouseClicked

    private void tbChiTietNhapKhoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbChiTietNhapKhoMouseClicked
        // TODO add your handling code here:
        int index = tbChiTietNhapKho.getSelectedRow();
        HienThiChiTietPhieuNhap(listChiTietPhieuNhapKho.get(index));
    }//GEN-LAST:event_tbChiTietNhapKhoMouseClicked

    private void cbNhaCungCapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNhaCungCapActionPerformed
        // TODO add your handling code here:
        int index = cbNhaCungCap.getSelectedIndex() - 1;
        if (index == -1) {
            return;
        }
//        previuos = listNhaCungCap.get(index);
        LoadSanPham(listNhaCungCap.get(index));

    }//GEN-LAST:event_cbNhaCungCapActionPerformed

    private void btnThemSanPhamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemSanPhamActionPerformed
        int index = cbSanPham.getSelectedIndex() - 1;
        if (index < 0) {
            JOptionPane.showMessageDialog(null, "Sản phẩm không hợp lệ");
            return;
        }

        Sanpham sp = listSanPham.get(index);
        if (!KiemTraSoLuongNhap(sp)) {
            System.out.println("Sản phẩm không hợp lệ");
            return;
        }

        if (tbPhieuNhapKho.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Chưa chọn phiếu nhập kho");
            return;
        }
        for (Chitietphieunhapkho item : listChiTietPhieuNhapKho) {
            if (item.getSanpham().getTensanpham().equals(sp.getTensanpham())) {
                JOptionPane.showMessageDialog(null, "Sản phẩm này đã được nhập");
                return;
            }
        }
        Phieunhapkho pnk = listPhieuNhapKho.get(tbPhieuNhapKho.getSelectedRow());

        Chitietphieunhapkho ctpnk = new Chitietphieunhapkho();
        ctpnk.setSanpham(sp);
        ctpnk.setSoluong(Integer.parseInt(tfSoLuong.getText()));
        ctpnk.setDongianhap(Float.parseFloat(tfDonGia.getText()));
        ctpnk.setPhieunhapkho(pnk);

        new ChiTietPhieuNhapKhoDAO().ThemChiTietPhieuNhapKho(ctpnk);
        listChiTietPhieuNhapKho.add(ctpnk);
        SetChiTietPhieuNhapKho(listChiTietPhieuNhapKho);

    }//GEN-LAST:event_btnThemSanPhamActionPerformed

    private void btnXoaSanPhamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaSanPhamActionPerformed
        int index = cbSanPham.getSelectedIndex() - 1;
        if (index < 0) {
            JOptionPane.showMessageDialog(null, "Sản phẩm không hợp lệ");
            return;
        }
        Sanpham sp = listSanPham.get(index);
        if (!KiemTraSoLuongNhap(sp)) {
            JOptionPane.showMessageDialog(null, "Sản phẩm không hợp lệ");
            return;
        }

        if (tbPhieuNhapKho.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Chưa chọn phiếu nhập kho");
            return;
        }

        Phieunhapkho pnk = listPhieuNhapKho.get(tbPhieuNhapKho.getSelectedRow());
        if (tbChiTietNhapKho.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Bạn chưa chọn sản phẩm");
            return;
        }
        Chitietphieunhapkho ctpnk = listChiTietPhieuNhapKho.get(tbChiTietNhapKho.getSelectedRow());

        if (!ctpnk.getSanpham().getTensanpham().equals(sp.getTensanpham())) {
            JOptionPane.showMessageDialog(null, "Sản phẩm không được thay đổi");
            return;
        }
        ctpnk.setSanpham(sp);
        ctpnk.setSoluong(Integer.parseInt(tfSoLuong.getText()));
        ctpnk.setDongianhap(Float.parseFloat(tfDonGia.getText()));
        ctpnk.setPhieunhapkho(pnk);

        new ChiTietPhieuNhapKhoDAO().XoaChiTietPhieuNhapKho(ctpnk);

        listChiTietPhieuNhapKho.remove(index);
        SetChiTietPhieuNhapKho(listChiTietPhieuNhapKho);
    }//GEN-LAST:event_btnXoaSanPhamActionPerformed

    private void btnThemPhieuNhapKhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemPhieuNhapKhoActionPerformed
        // TODO add your handling code here:
        PhieuNhapKhoFrame frm = new PhieuNhapKhoFrame(this, null, nhanVien);
        frm.setVisible(true);
    }//GEN-LAST:event_btnThemPhieuNhapKhoActionPerformed

    private void btnSuaSanPhamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaSanPhamActionPerformed
        // TODO add your handling code here:
        int index = cbSanPham.getSelectedIndex() - 1;
        if (index < 0) {
            JOptionPane.showMessageDialog(null, "Sản phẩm không hợp lệ");
            return;
        }
        Sanpham sp = listSanPham.get(index);
        if (!KiemTraSoLuongNhap(sp)) {
            JOptionPane.showMessageDialog(null, "Sản phẩm không hợp lệ");
            return;
        }

        if (tbPhieuNhapKho.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Chưa chọn phiếu nhập kho");
            return;
        }

        Phieunhapkho pnk = listPhieuNhapKho.get(tbPhieuNhapKho.getSelectedRow());
        if (tbChiTietNhapKho.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Bạn chưa chọn sản phẩm");
            return;
        }
        Chitietphieunhapkho ctpnk = listChiTietPhieuNhapKho.get(tbChiTietNhapKho.getSelectedRow());

        if (!ctpnk.getSanpham().getTensanpham().equals(sp.getTensanpham())) {
            JOptionPane.showMessageDialog(null, "Sản phẩm không được thay đổi");
            return;
        }
        ctpnk.setSanpham(sp);
        ctpnk.setSoluong(Integer.parseInt(tfSoLuong.getText()));
        ctpnk.setDongianhap(Float.parseFloat(tfDonGia.getText()));
        ctpnk.setPhieunhapkho(pnk);

        new ChiTietPhieuNhapKhoDAO().SuaChiTietPhieuNhapKho(ctpnk);
        SetChiTietPhieuNhapKho(listChiTietPhieuNhapKho);

    }//GEN-LAST:event_btnSuaSanPhamActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSuaSanPham;
    private javax.swing.JButton btnThayDoiPhieuNhapKho;
    private javax.swing.JButton btnThemPhieuNhapKho;
    private javax.swing.JButton btnThemSanPham;
    private javax.swing.JButton btnXoaSanPham;
    private javax.swing.JComboBox cbNhaCungCap;
    private javax.swing.JComboBox cbSanPham;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbChiTietNhapKho;
    private javax.swing.JTable tbPhieuNhapKho;
    private javax.swing.JTextField tfDonGia;
    private javax.swing.JTextField tfMaPhieuNhap;
    private com.toedter.calendar.JDateChooser tfNgayNhap;
    private javax.swing.JTextField tfSoLuong;
    // End of variables declaration//GEN-END:variables

    private Nhanvien nhanVien;
    private boolean flagThem = false;
    private ArrayList<Phieunhapkho> listPhieuNhapKho;
    private ArrayList<Chitietphieunhapkho> listChiTietPhieuNhapKho = null;
    private ArrayList<Nhacungcap> listNhaCungCap;
    private ArrayList<Sanpham> listSanPham;

    private DefaultTableModel CreatePhieuNhapKho() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Mã phiếu nhập");
        model.addColumn("Ngày nhập");
        model.addColumn("Người nhập");
        return model;
    }

    private DefaultTableModel CreateChiTietPhieuNhapKho() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Tên sản phẩm");
        model.addColumn("Số lượng");
        model.addColumn("Đơn giá");
        return model;
    }

    public void LoadPhieuNhapKho() {
        DefaultTableModel model = CreatePhieuNhapKho();
        listPhieuNhapKho = new PhieuNhapKhoDAO().LayDSPhieuNhapKho();
        for (Phieunhapkho pnk : listPhieuNhapKho) {
            Vector vt = new Vector();
            vt.add(pnk.getMaphieunhap());
            vt.add(pnk.getNgaynhap());
            vt.add(pnk.getNhanvien().getTennhanvien());
            model.addRow(vt);
        }
        tbPhieuNhapKho.setModel(model);
    }

    private void LoadNhanCungCap() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        listNhaCungCap = new NhaCungCapDAO().LayDSNhaCungCap();
        model.addElement("Chọn nhà cung cấp");
        for (Nhacungcap ncc : listNhaCungCap) {
            model.addElement(ncc.getTennhacungcap());
        }
        cbNhaCungCap.setModel(model);
    }

    private void LoadChiTietPhieuNhapKho(Phieunhapkho pnk) {
        listChiTietPhieuNhapKho = new ChiTietPhieuNhapKhoDAO().LayDSChiTietPhieuNhapKho(pnk);
        SetChiTietPhieuNhapKho(listChiTietPhieuNhapKho);
    }

    private void LoadSanPham(Nhacungcap ncc) {
        listSanPham = new SanPhamDAO().LayDSSanPhamTuNhaCungCap(ncc);
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement("Chọn sản phẩm");
        for (Sanpham sp : listSanPham) {
            model.addElement(sp.getTensanpham());
        }
        cbSanPham.setModel(model);
    }

    private void SetChiTietPhieuNhapKho(ArrayList<Chitietphieunhapkho> listChiTietPhieuNhapKho) {
        DefaultTableModel model = CreateChiTietPhieuNhapKho();
        for (Chitietphieunhapkho ctpnk : listChiTietPhieuNhapKho) {
            Vector vt = new Vector();
            vt.add(ctpnk.getSanpham().getTensanpham());
            vt.add(ctpnk.getSoluong());
            vt.add(ctpnk.getDongianhap());
            model.addRow(vt);
        }
        tbChiTietNhapKho.setModel(model);
    }

    private boolean KiemTraSoLuongNhap(Sanpham sp) {
        if (sp.getSoluongtontoithieu() < sp.getSoluongton()) {
            return false;
        }
        return sp.getSoluongton() + Integer.parseInt(tfSoLuong.getText()) <= sp.getSoluongtontoida();
    }

    private void HienThiChiTietPhieuNhap(Chitietphieunhapkho ctpnk) {
        cbSanPham.setSelectedItem(ctpnk.getSanpham().getTensanpham());
        tfSoLuong.setText(ctpnk.getSoluong().toString());
        tfDonGia.setText(Float.toString(ctpnk.getDongianhap()));
    }

    private void ThemPhieuNhapKho() {
//        flagThem = true;
//        tfMaPhieuNhap.setEnabled(true);
//        tfMaPhieuNhap.setText("");
//        tfNgayNhap.("");
//        cbNhaCungCap.setSelectedIndex(0);
//        tfSoLuong.setText("");
//        tfDonGia.setText("");
//        cbSanPham.setSelectedIndex(0);
//        listChiTietPhieuNhapKho = new ArrayList<>();
//        DefaultTableModel model = CreateChiTietPhieuNhapKho();
//        tbChiTietNhapKho.setModel(model);
    }

//    private void LuuThongTin(Phieunhapkho pnk, ArrayList<Chitietphieunhapkho> listChiTietPhieuNhapKho) {
//        try {
//            Date ngay = new Date();
//            Nhacungcap ncc = (Nhacungcap) cbNhaCungCap.getSelectedItem();
//            pnk.setNgaycapnhat(ngay);
//            pnk.setNhacungcap(ncc);
//            pnk.setMaphieunhap(tfMaPhieuNhap.getText());
//            pnk.setNgaynhap(new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).parse(tfNgayNhap.getText()));
//            if (flagThem) {
//                pnk.setNgaytaophieu(ngay);
//                pnk.setNhanvien(nhanVien);
////                int id = PhieuNhapKhoDAO.ThemPhieuNhapKho();
////                pnk.setId(id);
//
//            } else {
////                PhieuNhapKhoDAO.ThayDoiPhieuNhapKho();
////                ChiTietPhieuNhapKhoDAO.XoaChiTietPhieuNhapKho(pnk);
//            }
//            for (Chitietphieunhapkho ctpnk : listChiTietPhieuNhapKho) {
//                ctpnk.setPhieunhapkho(pnk);
////                ChiTietPhieuNhapKhoDAO.ThemChiTietPhieuNhapKho();
//            }
//        } catch (ParseException ex) {
//            Logger.getLogger(NhapKhoHangPanel.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
}
