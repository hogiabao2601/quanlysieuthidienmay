/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Nhacungcap;
import POJO.Sanpham;
import Utils.DB_Utils;
import java.util.ArrayList;

/**
 *
 * @author Ho Gia Bao
 */
public class SanPhamDAO {

    private final DB_Utils dbUtils;

    public SanPhamDAO() {
        dbUtils = new DB_Utils();
    }

    public ArrayList<Sanpham> LayDSSanPham() {
        return (ArrayList<Sanpham>) dbUtils.Select("FROM Sanpham");
    }

    public ArrayList<Sanpham> LayDSSanPhamTuNhaCungCap(Nhacungcap ncc) {
        return (ArrayList<Sanpham>) dbUtils.Select("FROM Sanpham sp WHERE sp.nhacungcap=" + ncc.getId());
    }

}
