/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Chitietphieuxuatkho;
import POJO.Phieuxuatkho;
import Utils.DB_Utils;
import java.util.ArrayList;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Ho Gia Bao
 */
public class ChiTietPhieuXuatKhoDAO {

    public ChiTietPhieuXuatKhoDAO() {
    }

    public ArrayList<Chitietphieuxuatkho> LayDSChiTietPhieuXuatKho(Phieuxuatkho pxk) {
        return (ArrayList<Chitietphieuxuatkho>) new DB_Utils().Select("FROM Chitietphieuxuatkho");
    }
    
    public Chitietphieuxuatkho LayChiTietPhieuXuatKho(String maPhieuXuatKho) {
        Chitietphieuxuatkho chiTietPhieuXuatKho = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        try {
            chiTietPhieuXuatKho = (Chitietphieuxuatkho) session.get(Chitietphieuxuatkho.class, maPhieuXuatKho);
        } catch (HibernateException e) {
            System.err.println(e);
        } finally {
            session.close();
        }
        return chiTietPhieuXuatKho;
    }
}
