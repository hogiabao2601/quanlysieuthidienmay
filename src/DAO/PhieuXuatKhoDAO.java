/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Phieunhapkho;
import POJO.Phieuxuatkho;
import Utils.DB_Utils;
import java.util.ArrayList;

/**
 *
 * @author Ho Gia Bao
 */
public class PhieuXuatKhoDAO {

    private final DB_Utils dbUtils;

    public PhieuXuatKhoDAO() {
        dbUtils = new DB_Utils();
    }

    public ArrayList<Phieuxuatkho> LayDSPhieuXuatKho() {
        return (ArrayList<Phieuxuatkho>) dbUtils.Select("FROM Phieunhapkho");
    }

    public boolean ThemPhieuXuatKho(Phieuxuatkho phieuXuatKho) {
        return dbUtils.Insert(phieuXuatKho);
    }
    
    public boolean SuaPhieuXuatKho(Phieuxuatkho phieuXuatKho) {
        return dbUtils.Update(phieuXuatKho);
    }
}
