/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Phieunhapkho;
import java.util.ArrayList;
import Utils.DB_Utils;
import net.sf.ehcache.hibernate.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Ho Gia Bao
 */
public class PhieuNhapKhoDAO {

    private DB_Utils dbUtils;

    public PhieuNhapKhoDAO() {
        dbUtils = new DB_Utils();
    }

    public ArrayList<Phieunhapkho> LayDSPhieuNhapKho() {
        ArrayList<Phieunhapkho> listPhieuNhapKho = null;
        listPhieuNhapKho = (ArrayList<Phieunhapkho>) dbUtils.Select("FROM Phieunhapkho");
        return listPhieuNhapKho;
    }

    public Phieunhapkho LayPhieuNhapKho(String maPhieuNhap) {
        Phieunhapkho phieuNhapKho = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        try {
            phieuNhapKho = (Phieunhapkho) session.get(Phieunhapkho.class, maPhieuNhap);
        } catch (HibernateException e) {
            System.err.println(e);
        } finally {
            session.close();
        }
        return phieuNhapKho;
    }

    public void ThemPhieuNhapKho(Phieunhapkho phieuNhapKho) {
        dbUtils.Insert(phieuNhapKho);
    }

    public void CapNhatPheuNhapKho(Phieunhapkho phieuNhapKho) {
        dbUtils.Update(phieuNhapKho);
    }
}
