/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Loainhanvien;
import POJO.Nhatky;
import Utils.DB_Utils;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Ho Gia Bao
 */
public class LoaiNhanVienDAO {
    
    private DB_Utils dbUtils ;
    public LoaiNhanVienDAO() {
        dbUtils = new DB_Utils();
    }
    public ArrayList<Loainhanvien> LayDSLoaiNhanVien() {
        ArrayList<Loainhanvien> listLoaiNhanVien = null;
        listLoaiNhanVien = (ArrayList<Loainhanvien>)dbUtils.Select("FROM Loainhanvien");
        return listLoaiNhanVien;
    }
    
}
