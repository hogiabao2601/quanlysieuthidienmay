/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Nhacungcap;
import Utils.DB_Utils;
import java.util.ArrayList;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Ho Gia Bao
 */
public class NhaCungCapDAO {

    private DB_Utils dbUtils;

    public NhaCungCapDAO() {
        dbUtils = new DB_Utils();
    }

    public ArrayList<Nhacungcap> LayDSNhaCungCap() {
        return (ArrayList<Nhacungcap>) dbUtils.Select("FROM Nhacungcap WHERE Trangthai = true");
    }

    public boolean ThemNhaCungCap(Nhacungcap obj) {
        return dbUtils.Insert(obj);
    }

    public boolean SuaNhaCungCap(Nhacungcap obj) {
        return dbUtils.Update(obj);
    }
    
    public Nhacungcap LayNhaCungCap(String maNhaCungCap) {
        Nhacungcap nhaCungCap = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        try {
            nhaCungCap = (Nhacungcap) session.get(Nhacungcap.class, maNhaCungCap);
        } catch (HibernateException e) {
            System.err.println(e);
        } finally {
            session.close();
        }
        return nhaCungCap;
    }
}
