/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Nhanvien;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Ho Gia Bao
 */
public class NhanVienDAO {

    public Nhanvien LayThongTinNV(String username, String password) {
        String hql = "FROM Nhanvien nv WHERE nv.manhanvien='" + username + "' AND nv.matkhau = '" + password+"'";
        SessionFactory factory = NewHibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Query query =  session.createQuery(hql);
        return (Nhanvien)query.uniqueResult();
    }
    
    public Nhanvien LayThongTinNV() {
        String hql = "FROM Nhanvien";
        SessionFactory factory = NewHibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Query query =  session.createQuery(hql);
        return (Nhanvien)query.uniqueResult();
    }
}
