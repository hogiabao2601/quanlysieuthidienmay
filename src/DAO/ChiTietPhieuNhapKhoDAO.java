/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Chitietphieunhapkho;
import POJO.Phieunhapkho;
import Utils.DB_Utils;
import java.util.ArrayList;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Ho Gia Bao
 */
public class ChiTietPhieuNhapKhoDAO {

    private DB_Utils dbUtils;

    public ChiTietPhieuNhapKhoDAO() {
        dbUtils = new DB_Utils();
    }

    public ArrayList<Chitietphieunhapkho> LayDSChiTietPhieuNhapKho(Phieunhapkho pnk) {
        ArrayList<Chitietphieunhapkho> listChitietphieunhapkho = null;
        String hql = "FROM Chitietphieunhapkho ctpnk WHERE ctpnk.phieunhapkho=" + pnk.getId();
        listChitietphieunhapkho = (ArrayList<Chitietphieunhapkho>) dbUtils.Select(hql);
        return listChitietphieunhapkho;
    }

    public boolean ThemChiTietPhieuNhapKho(Chitietphieunhapkho item) {
        return dbUtils.Insert(item);
    }

    public void XoaChiTietPhieuNhapKho(Chitietphieunhapkho ctpnk) {
        SessionFactory factory = NewHibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        org.hibernate.Transaction transaction = null;
        try {
            ctpnk.setTrangthai(false);
            transaction = session.beginTransaction();
            session.update(ctpnk);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            System.out.println(e);
        } finally {
            session.close();
        }
    }

    public boolean SuaChiTietPhieuNhapKho(Chitietphieunhapkho ctpnk) {
        return dbUtils.Update(ctpnk);
    }

}
