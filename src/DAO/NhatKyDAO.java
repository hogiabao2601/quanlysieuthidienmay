/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Nhatky;
import Utils.DB_Utils;
import java.util.ArrayList;

/**
 *
 * @author Ho Gia Bao
 */
public class NhatKyDAO {
    private DB_Utils dbUtils ;
    public NhatKyDAO() {
        dbUtils = new DB_Utils();
    }
    public ArrayList<Nhatky> LayDSNhatKy() {
        ArrayList<Nhatky> listNhatKy = null;
        listNhatKy = (ArrayList<Nhatky>)dbUtils.Select("FROM Nhatky");
        return listNhatKy;
    }
}
