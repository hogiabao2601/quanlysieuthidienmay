/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Loaitinhtrang;
import Utils.DB_Utils;
import java.util.ArrayList;

public class TrangThaiDAO {

    private final DB_Utils dbUtils;

    public TrangThaiDAO() {
        dbUtils = new DB_Utils();
    }

    public boolean ThemLoaiTrangThai(Loaitinhtrang l) {
        return dbUtils.Insert(l);
    }

    public boolean SuaLoaiTrangThai(Loaitinhtrang l) {
        return dbUtils.Update(l);
    }

    public ArrayList<Loaitinhtrang> LayDSTinhTrang() {
        return (ArrayList<Loaitinhtrang>) dbUtils.Select("FROM Loaitinhtrang");
    }
}
