package POJO;
// Generated Oct 4, 2014 9:18:04 PM by Hibernate Tools 3.6.0


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Nhanvien generated by hbm2java
 */
public class Nhanvien  implements java.io.Serializable {


     private int id;
     private Loainhanvien loainhanvien;
     private Cuahang cuahang;
     private String manhanvien;
     private String tennhanvien;
     private Integer gioitinh;
     private String matkhau;
     private Date ngaysinh;
     private String diachi;
     private String sodienthoai;
     private Boolean trangthai;
     private Set phieunhapkhos = new HashSet(0);
     private Set hoadonsForManhanvienlaphoadon = new HashSet(0);
     private Set phieuxuatkhosForManhanvienlapphieu = new HashSet(0);
     private Set hoadonsForManhanvienban = new HashSet(0);
     private Set phieuxuatkhosForManhanviengiaohang = new HashSet(0);

    public Nhanvien() {
    }

	
    public Nhanvien(int id) {
        this.id = id;
    }
    public Nhanvien(int id, Loainhanvien loainhanvien, Cuahang cuahang, String manhanvien, String tennhanvien, Integer gioitinh, String matkhau, Date ngaysinh, String diachi, String sodienthoai, Boolean trangthai, Set phieunhapkhos, Set hoadonsForManhanvienlaphoadon, Set phieuxuatkhosForManhanvienlapphieu, Set hoadonsForManhanvienban, Set phieuxuatkhosForManhanviengiaohang) {
       this.id = id;
       this.loainhanvien = loainhanvien;
       this.cuahang = cuahang;
       this.manhanvien = manhanvien;
       this.tennhanvien = tennhanvien;
       this.gioitinh = gioitinh;
       this.matkhau = matkhau;
       this.ngaysinh = ngaysinh;
       this.diachi = diachi;
       this.sodienthoai = sodienthoai;
       this.trangthai = trangthai;
       this.phieunhapkhos = phieunhapkhos;
       this.hoadonsForManhanvienlaphoadon = hoadonsForManhanvienlaphoadon;
       this.phieuxuatkhosForManhanvienlapphieu = phieuxuatkhosForManhanvienlapphieu;
       this.hoadonsForManhanvienban = hoadonsForManhanvienban;
       this.phieuxuatkhosForManhanviengiaohang = phieuxuatkhosForManhanviengiaohang;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Loainhanvien getLoainhanvien() {
        return this.loainhanvien;
    }
    
    public void setLoainhanvien(Loainhanvien loainhanvien) {
        this.loainhanvien = loainhanvien;
    }
    public Cuahang getCuahang() {
        return this.cuahang;
    }
    
    public void setCuahang(Cuahang cuahang) {
        this.cuahang = cuahang;
    }
    public String getManhanvien() {
        return this.manhanvien;
    }
    
    public void setManhanvien(String manhanvien) {
        this.manhanvien = manhanvien;
    }
    public String getTennhanvien() {
        return this.tennhanvien;
    }
    
    public void setTennhanvien(String tennhanvien) {
        this.tennhanvien = tennhanvien;
    }
    public Integer getGioitinh() {
        return this.gioitinh;
    }
    
    public void setGioitinh(Integer gioitinh) {
        this.gioitinh = gioitinh;
    }
    public String getMatkhau() {
        return this.matkhau;
    }
    
    public void setMatkhau(String matkhau) {
        this.matkhau = matkhau;
    }
    public Date getNgaysinh() {
        return this.ngaysinh;
    }
    
    public void setNgaysinh(Date ngaysinh) {
        this.ngaysinh = ngaysinh;
    }
    public String getDiachi() {
        return this.diachi;
    }
    
    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }
    public String getSodienthoai() {
        return this.sodienthoai;
    }
    
    public void setSodienthoai(String sodienthoai) {
        this.sodienthoai = sodienthoai;
    }
    public Boolean getTrangthai() {
        return this.trangthai;
    }
    
    public void setTrangthai(Boolean trangthai) {
        this.trangthai = trangthai;
    }
    public Set getPhieunhapkhos() {
        return this.phieunhapkhos;
    }
    
    public void setPhieunhapkhos(Set phieunhapkhos) {
        this.phieunhapkhos = phieunhapkhos;
    }
    public Set getHoadonsForManhanvienlaphoadon() {
        return this.hoadonsForManhanvienlaphoadon;
    }
    
    public void setHoadonsForManhanvienlaphoadon(Set hoadonsForManhanvienlaphoadon) {
        this.hoadonsForManhanvienlaphoadon = hoadonsForManhanvienlaphoadon;
    }
    public Set getPhieuxuatkhosForManhanvienlapphieu() {
        return this.phieuxuatkhosForManhanvienlapphieu;
    }
    
    public void setPhieuxuatkhosForManhanvienlapphieu(Set phieuxuatkhosForManhanvienlapphieu) {
        this.phieuxuatkhosForManhanvienlapphieu = phieuxuatkhosForManhanvienlapphieu;
    }
    public Set getHoadonsForManhanvienban() {
        return this.hoadonsForManhanvienban;
    }
    
    public void setHoadonsForManhanvienban(Set hoadonsForManhanvienban) {
        this.hoadonsForManhanvienban = hoadonsForManhanvienban;
    }
    public Set getPhieuxuatkhosForManhanviengiaohang() {
        return this.phieuxuatkhosForManhanviengiaohang;
    }
    
    public void setPhieuxuatkhosForManhanviengiaohang(Set phieuxuatkhosForManhanviengiaohang) {
        this.phieuxuatkhosForManhanviengiaohang = phieuxuatkhosForManhanviengiaohang;
    }




}


