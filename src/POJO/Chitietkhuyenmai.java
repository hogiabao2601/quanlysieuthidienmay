package POJO;
// Generated Oct 4, 2014 9:18:04 PM by Hibernate Tools 3.6.0


import java.math.BigDecimal;

/**
 * Chitietkhuyenmai generated by hbm2java
 */
public class Chitietkhuyenmai  implements java.io.Serializable {


     private int id;
     private Sanpham sanpham;
     private Khuyenmai khuyenmai;
     private BigDecimal giatrigiam;
     private Boolean trangthai;

    public Chitietkhuyenmai() {
    }

	
    public Chitietkhuyenmai(int id) {
        this.id = id;
    }
    public Chitietkhuyenmai(int id, Sanpham sanpham, Khuyenmai khuyenmai, BigDecimal giatrigiam, Boolean trangthai) {
       this.id = id;
       this.sanpham = sanpham;
       this.khuyenmai = khuyenmai;
       this.giatrigiam = giatrigiam;
       this.trangthai = trangthai;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Sanpham getSanpham() {
        return this.sanpham;
    }
    
    public void setSanpham(Sanpham sanpham) {
        this.sanpham = sanpham;
    }
    public Khuyenmai getKhuyenmai() {
        return this.khuyenmai;
    }
    
    public void setKhuyenmai(Khuyenmai khuyenmai) {
        this.khuyenmai = khuyenmai;
    }
    public BigDecimal getGiatrigiam() {
        return this.giatrigiam;
    }
    
    public void setGiatrigiam(BigDecimal giatrigiam) {
        this.giatrigiam = giatrigiam;
    }
    public Boolean getTrangthai() {
        return this.trangthai;
    }
    
    public void setTrangthai(Boolean trangthai) {
        this.trangthai = trangthai;
    }




}


